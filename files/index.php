<?php
include('../config.php');
if ($PROTECT) {
    require_once('../protect.php');
}
error_reporting(E_ERROR);
?>

<html lang="en">

<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
    <title>Nyttig Files</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/css/uikit.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.5.7/dist/js/uikit-icons.min.js"></script>
</head>
<div class="uk-container uk-margin-small-top">
    <div class="uk-card uk-card-primary uk-card-body">
        <h1 class="uk-heading-line uk-text-center"><span>Nyttig Files</span></h1>
        <?php
        if (!is_dir($SUBDIR)) {
            mkdir($SUBDIR, 0705);
            chmod($SUBDIR, 0705);
            $h = fopen($SUBDIR . '/.htaccess', 'w') or die("Can't create .htaccess file.");
            fwrite($h, "Options -ExecCGI\nAddHandler cgi-script .php .pl .py .jsp .asp .htm .shtml .sh .cgi");
            fclose($h);
            $h = fopen($SUBDIR . '/index.html', 'w') or die("Can't create index.html file.");
            fwrite($h, '<html><head><meta http-equiv="refresh" content="0;url=' . $_SERVER["SCRIPT_NAME"] . '"></head><body></body></html>');
            fclose($h);
        }
        $scriptname = basename($_SERVER["SCRIPT_NAME"]);
        if (isset($_FILES['filetoupload'])) {
            $filename = $SUBDIR . '/' . basename($_FILES['filetoupload']['name']);
            if (file_exists($filename)) {
                print 'This file already exists.';
                exit();
            }
            if (move_uploaded_file($_FILES['filetoupload']['tmp_name'], $filename)) {
                $serverport = '';
                if ($_SERVER["SERVER_PORT"] != '80') {
                    $serverport = ':' . $_SERVER["SERVER_PORT"];
                }
                $fileurl = 'http://' . $_SERVER["SERVER_NAME"] . $serverport . dirname($_SERVER["SCRIPT_NAME"]). '/' . $SUBDIR . '/' . basename($_FILES['filetoupload']['name']);
                echo 'The file/image was uploaded to <a href="' . $fileurl . '">' . $fileurl . '</a><br/>';
            } else {
                echo "There was an error uploading the file, please try again !";
            }
            echo '<a class="uk-button uk-button-default uk-margin-top" href="' . $scriptname . '">Back</a>';
            exit();
        }
        print <<<EOD
<form method="post" action="$scriptname" enctype="multipart/form-data">        
    Choose file to upload: <input class="uk-input uk-margin-bottom" type="file" name="filetoupload" size="60">
    <input class="uk-button uk-button-primary uk-margin-top" type="submit" value="Share">
    <a class="uk-button uk-button-default uk-margin-top" href="../index.php">Back</a>
</form>
EOD;
        ?>
    </div>
</div>
</body>

</html>