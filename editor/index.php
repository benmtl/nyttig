<?php
include('../config.php');
if ($PROTECT) {
    require_once('../protect.php');
}
error_reporting(E_ERROR);
?>
<html lang='en'>

    <!-- Author: Dmitri Popov, dmpop@linux.com
		 License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->
		 
    <head>
	<title>Nyttig Editor</title>
	<meta charset="utf-8">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/uikit.min.css" />
	<script src="../js/uikit.min.js"></script>
	<script src="../js/uikit-icons.min.js"></script>
	<style>
		textarea {
			font-size: 15px;
			width: 100%;
			height: 55%;
			line-height: 1.9;
			margin-top: 2em;
		}
	</style>
    </head>
    <body>
	<div class="uk-container uk-margin-small-top">
		<div class="uk-card uk-card-default uk-card-body">
			<h1 class="uk-heading-line uk-text-center"><span>Nyttig Editor</span></h1>
            <?php
	    if (!file_exists($dir)) {
		mkdir($dir, 0777, true);
	    }
	    $mdfile = $dir."/content.md";
            function Read() {
		include('../config.php');
		$mdfile = $dir."/content.md";
                echo file_get_contents($mdfile);
            }
            function Write() {
		include('../config.php');
		$mdfile = $dir."/content.md";
		copy($mdfile, $dir.'/'.date('Y-m-d-H-i-s').'.md');
                $fp = fopen($mdfile, "w");
                $data = $_POST["text"];
                fwrite($fp, $data);
                fclose($fp);
            }
            ?>
            <?php
            if ($_POST["save"]){
		Write();
            };
            ?>
	    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
		<textarea class="uk-textarea" name="text"><?php Read(); ?></textarea>
		<input class="uk-button uk-button-primary uk-margin-top" type="submit" name="save" value="Save">
		<a class="uk-button uk-button-default uk-margin-top" href="../index.php">Back</a>
		</form>
	</div>
    </body>
</html>
