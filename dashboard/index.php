<html lang="en">

<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
	<title>Nyttig Dashboard</title>
	<meta charset="utf-8">
	<meta http-equiv="refresh" content="15">
	<link rel="shortcut icon" href="favicon.png" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/uikit.min.css" />
	<script src="../js/uikit.min.js"></script>
	<script src="../js/uikit-icons.min.js"></script>
	<script src="justgage.js"></script>
	<script src="raphael.min.js"></script>
</head>

<body>
	<?php
	$temp = shell_exec('cat /sys/class/thermal/thermal_zone*/temp');
	$temp = round($temp / 1000, 1);
	$cpuusage = 100 - shell_exec("vmstat | tail -1 | awk '{print $15}'");
	$mem = shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'");
	$mem = round($mem, 1);
	?>
	<div class="uk-container uk-container-small uk-margin-top">
		<h1 class="uk-heading-line uk-text-center"><span>Dashboard</span></h1>
		<div class="uk-child-width-expand@s uk-text-center" uk-grid>
			<div>
				<div class="uk-padding uk-text-center">
					<?php
					if (isset($temp) && is_numeric($temp)) { ?>
						<div id="tempgauge"></div>
						<script>
							var t = new JustGage({
								id: "tempgauge",
								value: <?php echo $temp; ?>,
								min: 0,
								max: 100,
								height: 200,
								width: 200,
								title: "Temperature",
								label: "°C"
							});
						</script>
					<?php } ?>
				</div>
			</div>
			<div>
				<div class="uk-padding uk-text-center">
					<?php if (isset($cpuusage) && is_numeric($cpuusage)) { ?>
						<div id="cpugauge"></div>
						<script>
							var u = new JustGage({
								id: "cpugauge",
								value: <?php echo $cpuusage; ?>,
								min: 0,
								max: 100,
								height: 200,
								width: 200,
								title: "CPU Load",
								label: "%"
							});
						</script>

					<?php } ?>
				</div>
			</div>
			<div>
				<div class="uk-padding uk-text-center"><?php if (isset($mem) && is_numeric($mem)) { ?>
						<div id="memgauge"></div>
						<script>
							var u = new JustGage({
								id: "memgauge",
								value: <?php echo $mem; ?>,
								min: 0,
								max: 100,
								height: 200,
								width: 200,
								title: "Memory",
								label: "%"
							});
						</script>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php
		if (isset($_POST['reboot'])) {
			echo "<script>";
			echo "UIkit.notification({message: 'Reboot initiated'});";
			echo "</script>";
			shell_exec('sudo reboot > /dev/null 2>&1 & echo $!');
		}
		?>
		<form class="uk-margin-top uk-margin-bottom uk-text-center" action=" " method="POST">
			<?php
			$str=shell_exec('cat /etc/*-release');
			if (strpos($str, 'Raspbian')) {
				echo '<a class="uk-button uk-button-danger" href="../bye.php">Shut down</a> ';
				echo '<button class="uk-button uk-button-primary" name="reboot">Reboot</button>';
			}
			?>
			<a class="uk-button uk-button-default" href="../index.php">Back</a>
		</form>
	</div>
</body>

</html>