<?php
include('config.php');
error_reporting(E_ERROR);
?>

<html lang="en">

<!-- Author: Dmitri Popov, dmpop@linux.com
         License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.txt -->

<head>
    <title>Nyttig</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/uikit.min.css" />
    <script src="js/uikit.min.js"></script>
    <script src="js/uikit-icons.min.js"></script>
</head>

<body>
    <div class="uk-container uk-container-xsmall uk-margin-top">
        <h1 class="uk-heading-line uk-text-center"><span>N Y T T I G</span></h1>
        <?php
        echo "<pre>";
        passthru("curl wttr.in/" . $CITY . "?0MTQ");
        echo "     <a href='https://wttr.in/'" . $CITY . "' target='_blank'>3-day forecast</a>";
        echo "</pre>";
        ?>
        <div class="uk-grid-collapse uk-child-width-expand@s uk-width-1-1@m uk-text-center" uk-grid>
            <div>
                <div class="uk-padding">
                    <a href="radio/"><img data-src="radio/favicon.png" width="" height="" alt="" uk-img></a>
                    <p>Radio</p>
                </div>
            </div>
            <div>
                <div class="uk-padding">
                    <a href="rss/"><img data-src="rss/favicon.png" width="" height="" alt="" uk-img></a>
                    <p>RSS</p>
                </div>
            </div>
            <div>
                <div class="uk-padding">
                    <a href="editor/"><img data-src="editor/favicon.png" width="" height="" alt="" uk-img></a>
                    <p>Editor</p>
                </div>
            </div>
        </div>
        <div class="uk-grid-collapse uk-child-width-expand@s uk-grid-match uk-width-1-1@m uk-text-center" uk-grid>
            <div>
                <div class="uk-padding">
                    <a href="files/"><img data-src="files/favicon.png" width="" height="" alt="" uk-img></a>
                    <p>Files</p>
                </div>
            </div>
            <div>
                <div class="uk-padding">
                    <a href="dashboard/"><img data-src="dashboard/favicon.png" width="" height="" alt="" uk-img></a>
                    <p>Dashboard</p>
                </div>
            </div>
            <div>
            <div class="uk-padding">
                    <!-- Empty tile -->
                </div>
            </div>
        </div>
        <!-- Uncomment to add custom tiles
        <div class="uk-grid-collapse uk-child-width-expand@s uk-grid-match uk-width-1-1@m uk-text-center" uk-grid>
            <div>
                <div class="uk-padding">Item 1</div>
            </div>
            <div>
                <div class="uk-padding">Item 2</div>
            </div>
            <div>
                <div class="uk-padding">Item 3</div>
            </div>
        </div> -->
        <hr>
        <p class="uk-text-center uk-margin-bottom">This is <a href="https://gitlab.com/dmpop/nyttig">Nyttig</a></p>
    </div>
</body>

</html>